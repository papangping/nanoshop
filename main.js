function init() {
  "use strict";

  var questionNumber;
  var lang = "th";

  var Sound = function(name, link, lang){
    this.name = name;
    this.link = link;
    this.lang = lang;
    this.audio = new Audio(this.link);

    var that = this;
    this.audio.onended = function(e){ if(typeof that.onended == "function") that.onended(e); };
  };
  Sound.prototype.onended = null;

  var SoundManager = {};
  SoundManager.soundList = [];
  SoundManager.play = function(name, lang){
    $.each(SoundManager.soundList, function(i, item){ item.audio.pause(); item.audio.currentTime = 0; });
    var sound = $.grep(SoundManager.soundList, function(item){ return item.name == name && item.lang == lang; })[0];
    sound.audio.play();
  };
  SoundManager.get = function(name, lang){
    return $.grep(SoundManager.soundList, function(item){ return item.name == name && item.lang == lang; })[0];
  };
  SoundManager.add = function(name, link, lang){
    var sound = new Sound(name, link, lang);
    SoundManager.soundList.push(sound);
  };
  SoundManager.init = function(){
    SoundManager.add("bg", "sounds/BG.mp3");
    var bg = SoundManager.get("bg");
    bg.audio.volume = 0.3;
    bg.audio.loop = true;

    SoundManager.add("applause", "sounds/Applause.mp3");
    SoundManager.add("ding", "sounds/Ding.mp3");
    SoundManager.add("true", "sounds/True.mp3");
    SoundManager.add("wrong", "sounds/Wrong.mp3");
    // bg.audio.play();

    SoundManager.add("page1", "sounds/th/Page01.mp3", "th");
    SoundManager.add("page2", "sounds/th/Page02.mp3", "th");

    SoundManager.add("1-1", "sounds/th/Shirt01.mp3", "th");
    SoundManager.add("1-2", "sounds/th/Shirt02.mp3", "th");
    SoundManager.add("1-3", "sounds/th/Shirt03.mp3", "th");
    SoundManager.add("2-1", "sounds/th/Beer.mp3", "th");
    SoundManager.add("2-2", "sounds/th/Beer 2.mp3", "th");
    SoundManager.add("2-3", "sounds/th/Beer 3.mp3", "th");
    SoundManager.add("3-1", "sounds/th/Box.mp3", "th");
    SoundManager.add("3-2", "sounds/th/Box 2.mp3", "th");
    SoundManager.add("3-3", "sounds/th/Box 3.mp3", "th");
    SoundManager.add("4-1", "sounds/th/Sun screen.mp3", "th");
    SoundManager.add("4-2", "sounds/th/Sun screen 2.mp3", "th");
    SoundManager.add("4-3", "sounds/th/Sun screen 3.mp3", "th");
    SoundManager.add("5-1", "sounds/th/Paint.mp3", "th");
    SoundManager.add("5-2", "sounds/th/Paint 2.mp3", "th");
    SoundManager.add("5-3", "sounds/th/Paint 3.mp3", "th");
    SoundManager.add("6-1", "sounds/th/Lamp.mp3", "th");
    SoundManager.add("6-2", "sounds/th/Lamp 2.mp3", "th");
    SoundManager.add("6-3", "sounds/th/Lamp 3.mp3", "th");
    SoundManager.add("7-1", "sounds/th/Acne.mp3", "th");
    SoundManager.add("7-2", "sounds/th/Acne 2.mp3", "th");
    SoundManager.add("7-3", "sounds/th/Acne 3.mp3", "th");
    SoundManager.add("8-1", "sounds/th/Plaster.mp3", "th");
    SoundManager.add("8-2", "sounds/th/Plaster 2.mp3", "th");
    SoundManager.add("8-3", "sounds/th/Plaster 3.mp3", "th");
    SoundManager.add("9-1", "sounds/th/llipstick.mp3", "th");
    SoundManager.add("9-2", "sounds/th/llipstick 2.mp3", "th");
    SoundManager.add("9-3", "sounds/th/llipstick 3.mp3", "th");
    SoundManager.add("total-1", "sounds/th/Total01.mp3", "th");
    SoundManager.add("total-2", "sounds/th/Total02.mp3", "th");


    SoundManager.add("page1", "sounds/en/Page01 EN.mp3", "en");
    SoundManager.add("page2", "sounds/en/Page02 EN.mp3", "en");

    SoundManager.add("1-1", "sounds/en/Shirt EN.mp3", "en");
    SoundManager.add("1-2", "sounds/en/Shirt EN 2.mp3", "en");
    SoundManager.add("1-3", "sounds/en/Paint EN 3.mp3", "en");
    SoundManager.add("2-1", "sounds/en/Beer EN.mp3", "en");
    SoundManager.add("2-2", "sounds/en/Beer EN 2.mp3", "en");
    SoundManager.add("2-3", "sounds/en/Beer EN 3.mp3", "en");
    SoundManager.add("3-1", "sounds/en/Box EN.mp3", "en");
    SoundManager.add("3-2", "sounds/en/Box EN 2.mp3", "en");
    SoundManager.add("3-3", "sounds/en/Box EN 3.mp3", "en");
    SoundManager.add("4-1", "sounds/en/Sun Screen EN.mp3", "en");
    SoundManager.add("4-2", "sounds/en/Sun Screen EN 2.mp3", "en");
    SoundManager.add("4-3", "sounds/en/Sun Screen EN 3.mp3", "en");
    SoundManager.add("5-1", "sounds/en/Paint EN.mp3", "en");
    SoundManager.add("5-2", "sounds/en/Paint EN 2.mp3", "en");
    SoundManager.add("5-3", "sounds/en/Paint EN 3.mp3", "en");
    SoundManager.add("6-1", "sounds/en/Lamp EN.mp3", "en");
    SoundManager.add("6-2", "sounds/en/Lamp EN 2.mp3", "en");
    SoundManager.add("6-3", "sounds/en/Lamp EN 3.mp3", "en");
    SoundManager.add("7-1", "sounds/en/Acne EN.mp3", "en");
    SoundManager.add("7-2", "sounds/en/Acne EN 2.mp3", "en");
    SoundManager.add("7-3", "sounds/en/Acne EN 3.mp3", "en");
    SoundManager.add("8-1", "sounds/en/Plaster EN.mp3", "en");
    SoundManager.add("8-2", "sounds/en/Plaster EN 2.mp3", "en");
    SoundManager.add("8-3", "sounds/en/Plaster EN 3.mp3", "en");
    SoundManager.add("9-1", "sounds/en/Lipstick EN.mp3", "en");
    SoundManager.add("9-2", "sounds/en/Lipstick EN 2.mp3", "en");
    SoundManager.add("9-3", "sounds/en/Lipstick EN 3.mp3", "en");
    SoundManager.add("total-1", "sounds/en/Total01 EN.mp3", "en");
    SoundManager.add("total-2", "sounds/en/Total02 EN.mp3", "en");
    // SoundManager.play();
  };
  SoundManager.init();
  SoundManager.play("page1", lang);

  var PageManager = {};
  PageManager.pageDisplay = "welcome";
  PageManager.$pages = $(".page");
  PageManager.changeDisplay = function(_name){
    this.pageDisplay = _name;
    PageManager.$pages.addClass("hidden").filter("#"+_name).removeClass("hidden");
    if(_name == "welcome") SoundManager.play("page1", lang);
  };

  var Question = function(_answer){
    this.answer = _answer;
  };
  Question.prototype.correct = false;
  Question.prototype.score = null;
  Question.prototype.refresh = function(){
    this.score = null;
    this.correct = false;
  };
  Question.prototype.reply = function(_reply){
    if(this.answer == _reply) {
      this.correct = true;
      if(this.score === null)
        this.score = true;
      return true;
    }
    else {
      if(this.score === null)
        this.score = false;
      this.correct = false;
      return false;
    }
  };

  var QuestionManager = {};
  QuestionManager.questions = [];
  QuestionManager.init = function(){
    this.questions.push(new Question(1));
    this.questions.push(new Question(2));
    this.questions.push(new Question(3));
    this.questions.push(new Question(3));
    this.questions.push(new Question(2));
    this.questions.push(new Question(2));
    this.questions.push(new Question(1));
    this.questions.push(new Question(2));
    this.questions.push(new Question(3));
  };
  QuestionManager.init();

  function initParamsAndView(){
    questionNumber = 1;
  }
  initParamsAndView();

  $(".th-btn").click(function(e){
    lang = "th";
    $("#wrapper").removeClass("en").addClass("th");
    SoundManager.play("page1", lang);
  });
  $(".en-btn").click(function(e){
    lang = "en";
    $("#wrapper").removeClass("th").addClass("en");
    SoundManager.play("page1", lang);
  });

  $(".go-shop-btn, .go-shop-btn-2").click(function(e){
    e.preventDefault();
    PageManager.changeDisplay("shop");
    SoundManager.play("page2", lang);
    $("#shop .q-icon").addClass("invisible");
    $("#shop .q-icon").each(function(i, item){
      setTimeout(function(){
        $(item).removeClass("invisible");
        $(item).addClass("animated bounceIn2");
        $(item).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function(e){
          $(item).removeClass("animated bounceIn2");
        });
      }, 200 * i);
    });
  });

  $(".q-icon").click(function(e){
    e.preventDefault();

    if($(this).hasClass("correct")) return;

    questionNumber = $(this).attr("q-number");
    PageManager.changeDisplay("question");
    var className = $("#question").attr("class");
    var match = className.match(/question-\d+/);
    if(match)
      $("#question").removeClass(match[0]);

    $("#question").addClass("question-"+questionNumber);
    $("#question .q-choice").addClass("invisible");
    $("#question .q-head").addClass("animated bounceIn");

    SoundManager.play(questionNumber+"-1", lang);
    SoundManager.get(questionNumber+"-1", lang).onended = function(e){
      SoundManager.play(questionNumber+"-2", lang);
      this.onended = null;

      $("#question .q-choice").each(function(i, item){
        setTimeout(function(){
          $(item).removeClass("invisible");
          $(item).addClass("animated bounceIn");
          $(item).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function(e){
            $(item).removeClass("animated bounceIn");
          });
        }, 200 * i);
      });
    };
  });

  $(".q-icon-drag").bind("mousedown touchstart", function(e){
    e.preventDefault();

    SoundManager.play(questionNumber+"-3", lang);
    // SoundManager.get(questionNumber+"-3", lang).audio.currentTime = 0.5;

    var $that = $(this);
    $(window).bind("mousemove touchmove", function(e){
      e.preventDefault();

      var x, y;
      if(e.originalEvent instanceof TouchEvent) {
        x = e.originalEvent.changedTouches[0].pageX;
        y = e.originalEvent.changedTouches[0].pageY;
      }
      else {
        x = e.pageX;
        y = e.pageY;
      }

      if(!$that.hasClass("dragging")) $that.addClass("dragging");
      var css = {"top": y - 100, "left": x - 100};
      $that.css(css);
    });

    $(window).bind("mouseup touchend", function(e){
      e.preventDefault();

      // reset view
      $that.css({"top": "", "left": ""});
      $that.removeClass("dragging");

      // unbind event
      $(window).unbind("mousemove touchmove mouseup touchend");

      // if in area reply answer
      console.log(e);
      var x, y;
      if(e.originalEvent instanceof TouchEvent) {
        x = e.originalEvent.changedTouches[0].pageX;
        y = e.originalEvent.changedTouches[0].pageY;
      }
      else {
        x = e.pageX;
        y = e.pageY;
      }
      var $basket = $(".q-basket");
      var position = {"top": y, "left": x};
      console.log(position);
      var area = $basket.offset();
      area.bottom = area.top + $basket.height();
      area.right = area.left + $basket.width();

      if((position.top > area.top && position.top < area.bottom) &&
        (position.left > area.left && position.left < area.right)) {

        var choiceNumber = parseInt($that.attr("choice-number"));
        var question = QuestionManager.questions[questionNumber-1];

        // if incorrect back to question
        if(!question.reply(choiceNumber)) {
          SoundManager.play("wrong");
          $(".q-incorrect").show();
          setTimeout(function(){ $(".q-incorrect").hide(); }, 500);
          return;
        }

        // if correct go next
        SoundManager.play("true");
        $(".q-correct").show();
        setTimeout(function(){
          $(".q-correct").hide();
          $(".q-icon-"+questionNumber).addClass("correct");

          PageManager.changeDisplay("correct");
          SoundManager.play("applause");
          var className = $("#correct").attr("class");
          var match = className.match(/correct-\d+/);
          if(match)
            $("#correct").removeClass(match[0]);
          $("#correct").addClass("correct-"+questionNumber);

          var doLength = QuestionManager.questions.filter(function(item, i){ return item.score !== null; }).length;
          if(doLength == 9) {
            $(".keep-shop-btn").hide();
          }
        }, 1000);
      }
    });
  });

  $(".keep-shop-btn").click(function(e){
    e.preventDefault();
    // PageManager.changeDisplay("shop");
    $(".go-shop-btn").click();
  });

  $(".checkout-btn").click(function(e){
    e.preventDefault();
    PageManager.changeDisplay("sum");
    var doLength = QuestionManager.questions.filter(function(item, i){ return item.score !== null; }).length;
    var score = QuestionManager.questions.filter(function(item, i){ return item.score; }).length;
    $(".do-length").text(doLength).addClass("invisible");
    $(".score").text(score).addClass("invisible");

    SoundManager.get("total-1", lang).onended = function(e){
      $(".do-length").removeClass("invisible");
      $(".do-length").addClass("animated bounceIn2");
      $(".do-length").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function(e){
        $(".do-length").removeClass("animated bounceIn2");
      });
      this.onended = null;

      SoundManager.get("total-2", lang).onended = function(e){
        $(".score").removeClass("invisible");
        $(".score").addClass("animated bounceIn2");
        $(".score").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function(e){
          $(".score").removeClass("animated bounceIn2");
        });
        this.onended = null;
      };
      SoundManager.play("total-2", lang);
    };
    SoundManager.play("total-1", lang);
  });

  $(".back-to-shop-btn").click(function(e){
    e.preventDefault();
    PageManager.changeDisplay("welcome");

    for(var i = 0; i < QuestionManager.questions.length; i++) {
      QuestionManager.questions[i].refresh();
    }
    $(".q-icon").removeClass("correct");
    $(".keep-shop-btn").show();
  });

  var toWelcome;
  $(window).bind("touchstart touchend touchmove mousedown mouseup mousemove", function(e){
    clearTimeout(toWelcome);
    var bg = SoundManager.get("bg");
    bg.audio.pause();
    bg.audio.currentTime = 0;
    toWelcome = setTimeout(function(){
      PageManager.changeDisplay("welcome");

      for(var i = 0; i < QuestionManager.questions.length; i++) {
        QuestionManager.questions[i].refresh();
      }
      $(".q-icon").removeClass("correct");
      $(".keep-shop-btn").show();

      SoundManager.play("bg");
    }, 60000);
  });
}

(function(){
  "use strict";

  init();
})();
